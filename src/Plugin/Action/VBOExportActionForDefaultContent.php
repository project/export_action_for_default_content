<?php

namespace Drupal\export_action_for_default_content\Plugin\Action;

use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsPreconfigurationInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Action to export content to Default Content.
 *
 * If type is left empty, action will be selectable for all
 * entity types.
 *
 * @Action(
 *   id = "export_action_for_default_content",
 *   label = @Translation("VBO (Views Bulk Operation) Export action for Default Content."),
 *   type = "",
 *   confirm = TRUE,
 * )
 */
class VBOExportActionForDefaultContent extends ViewsBulkOperationsActionBase implements ViewsBulkOperationsPreconfigurationInterface {

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    /*
     * All config resides in $this->configuration.
     * Passed view rows will be available in $this->context.
     * Data about the view used to select results and optionally
     * the batch context are available in $this->context or externally
     * through the public getContext() method.
     * The entire ViewExecutable object  with selected result
     * rows is available in $this->view or externally through
     * the public getView() method.
     */

    // Initialise the exporter, and export.
    /** @var \Drupal\default_content\ExporterInterface $exporter */
    $exporter = \Drupal::service('default_content.exporter');
    $export = $exporter->exportContentWithReferences($entity->getEntityTypeId(), $entity->id(), $this->configuration['export_action_for_default_content_pre_config']);

  }

  /**
   * {@inheritdoc}
   */
  public function buildPreConfigurationForm(array $form, array $values, FormStateInterface $form_state) {
    $form['export_action_for_default_content_pre_config'] = [
      '#title' => $this->t('Module path relative to Drupal root\'s index.php (including /content/)'),
      '#description' => t('Example: modules/custom/my_content_module/content/'),
      '#type' => 'textfield',
      '#default_value' => isset($values['export_action_for_default_content_pre_config']) ? $values['export_action_for_default_content_pre_config'] : '',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    if (\Drupal::currentUser()->hasPermission('export content using default content')) {
      return TRUE;
    }
    return FALSE;
  }

}
